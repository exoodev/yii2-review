# Review.
========================

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
composer require --prefer-dist exoodev/yii2-review
```

or add

```json
"exoodev/yii2-review": "*"
```

to the require section of your composer.json.



Usage
-----

Example backend configuration:

```php
return [
    'modules' => [
        'review' => [
            'class' => 'exoo\review\Module',
            'isBackend' => true,
        ],
    ],
];
```

Example frontend configuration:

```php
return [
    'modules' => [
        'review' => [
            'class' => 'exoo\review\Module',
        ]
    ],
];
```

## Migration

```
yii migrate --migrationPath=@exoo/review/migrations
