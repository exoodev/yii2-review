<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\review\models\backend;

use yii\data\ActiveDataProvider;
use exoo\review\models\Review;

/**
 * ReviewSearch is the class used to search and filter reviews.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class ReviewSearch extends Review
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at'], 'integer'],
            [['author', 'email', 'phone'], 'string'],
        ];
    }

    /**
     * Returns instance of the class ActiveDataProvider with introduced search query.
     * @param array $params Search parameters.
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Review::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'author', $this->author])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['status' => $this->status]);

        return $dataProvider;
    }
}
