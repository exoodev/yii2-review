<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\review\models\backend;

use yii\data\ActiveDataProvider;
use exoo\review\models\Content;

/**
 * ContentSearch is the class used to search and filter content.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class ContentSearch extends Content
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            ['language', 'string'],
        ];
    }

    /**
     * Returns instance of the class ActiveDataProvider with introduced search query.
     * @param array $params Search parameters.
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Content::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id])
            ->andFilterWhere(['language' => $this->language]);

        return $dataProvider;
    }
}
