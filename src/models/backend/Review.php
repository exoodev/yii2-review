<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\review\models\backend;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * Review is the class for extension Review.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Review extends \exoo\review\models\Review
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    /**
     * @var string Review creation date.
     */
    private $_creationDate;

    /**
     * @var string Review creation time.
     */
    private $_creationTime;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            ['creationDate', 'date', 'format' => 'yyyy-MM-dd', 'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE]],
            ['creationTime', 'date', 'format' => 'HH:mm', 'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE]],
        ];
        return array_merge(parent::rules(), $rules);
    }

    /**
     * Returns the model creation date.
     * @return string Creation date.
     */
    public function getCreationDate()
    {
        if ($this->_creationDate === null) {
            $this->_creationDate = $this->isNewRecord ? time() : $this->created_at;
        }

        return $this->_creationDate;
    }

    /**
     * Sets creation date.
     * @param mixed $value
     */
    public function setCreationDate($value)
    {
        $this->_creationDate = $value;
    }

    /**
     * Returns the model creation time.
     * @return string Creation time.
     */
    public function getCreationTime()
    {
        if ($this->_creationTime === null) {
            $this->_creationTime = $this->isNewRecord ? time() : $this->created_at;
        }
        return $this->_creationTime;
    }

    /**
     * Sets creation time.
     * @param mixed $value
     */
    public function setCreationTime($value)
    {
        $this->_creationTime = $value;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->_creationDate && $this->_creationTime) {
                $this->created_at = Yii::$app->formatter->asTimestamp($this->_creationDate . ' ' . $this->_creationTime);
            }
            return true;
        } else {
            return false;
        }
    }
}
