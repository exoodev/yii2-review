<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\review\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use exoo\status\StatusTrait;

/**
 * Review is the class for extension Review.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Review extends \yii\db\ActiveRecord
{
    use StatusTrait;

    /**
     * Status of the review, if it is not active.
     */
    const STATUS_INACTIVE = 0;
    /**
     * Status of the review, if it is active.
     */
    const STATUS_ACTIVE = 1;
    /**
     * Status of the review, if it is on moderation.
     */
    const STATUS_MODERATION = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%review}}';
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new ReviewQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment', 'author'], 'required'],
            [['author'], 'string', 'max' => 255],
            ['phone', 'string', 'max' => 20],
            [['comment', 'answer'], 'string'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            ['email', 'email'],
            ['status', 'default', 'value' => self::STATUS_MODERATION],
            ['status', 'in', 'range' => array_keys(self::getStatuses())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('review', 'ID'),
            'author' => Yii::t('review', 'Author'),
            'phone' => Yii::t('review', 'Phone number'),
            'email' => Yii::t('review', 'Email'),
            'text' => Yii::t('review', 'Review'),
            'status' => Yii::t('review', 'Status'),
            'comment' => Yii::t('review', 'Comment'),
            'answer' => Yii::t('review', 'Answer'),
            'created_at' => Yii::t('review', 'Created at'),
            'updated_at' => Yii::t('review', 'Updated at'),
            'created_by' => Yii::t('review', 'Created by'),
            'updated_by' => Yii::t('review', 'Updated by'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
        ];
    }

    public static function getStatuses(): array
    {
        return [
            [
                'id' => self::STATUS_INACTIVE,
                'name' => Yii::t('review', 'Inactive'),
                'class' => 'off ex-text-grey-300',
            ],
            [
                'id' => self::STATUS_ACTIVE,
                'name' => Yii::t('review', 'Active'),
                'class' => 'on uk-text-success',
            ],
            [
                'id' => self::STATUS_MODERATION,
                'name' => Yii::t('review', 'Moderation'),
                'class' => 'on uk-text-warning',
            ],
        ];
    }

    /**
     * Returns a value indicating whether the current model is active.
     * @return boolean if model is active value will be `true`, else `false`.
     */
    public function getIsActive()
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function getIsModeration()
    {
        return $this->status === self::STATUS_MODERATION;
    }
}
