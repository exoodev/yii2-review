<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\review\models;

use Yii;
use yii\base\Model;

/**
 * ReviewSetting class for configuration management for the extension Review.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Settings extends Model
{
    /**
     * @var integer count of desplayed reviews.
     */
    public $count_on_page;
    /**
     * @var integer numder of columns.
     */
    public $count_column = 1;
    /**
     * @var boolean whether to show review after moderation.
     */
    public $moderation;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string the application name.
     */
    protected $appName = 'review';

    /**
     * Returns the validation rules for attributes.
     * @return array Validation rules.
     */
    public function rules()
    {
        return [
            [['count_on_page'], 'integer', 'min' => 1, 'max' => 30],
            [['count_column'], 'integer', 'min' => 1, 'max' => 6],
            ['moderation', 'boolean'],
            ['title', 'string', 'max' => 255],
        ];
    }

    /**
     * Returns the attribute labels.
     * @return array Attribute labels (name => label).
     */
    public function attributeLabels()
    {
        return [
            'count_on_page' => Yii::t('review', 'Count of reviews on page'),
            'count_column' => Yii::t('review', 'Number of columns'),
            'moderation' => Yii::t('review', 'Moderation of reviews'),
            'title' => Yii::t('review', 'Title'),
        ];
    }
}
