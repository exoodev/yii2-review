<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\review\models;

use Yii;

/**
 * This is the ActiveQuery class for [[Review]].
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class ContentQuery extends \yii\db\ActiveQuery
{
    /**
     * Language.
     *
     * @return $this
     */
    public function language()
    {
        if (Yii::$app->settings->get('system', 'frontendMultilanguage')) {
            $this->andWhere(['language' => Yii::$app->language]);
        }

        return $this;
    }
}
