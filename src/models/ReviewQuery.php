<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\review\models;

use Yii;

/**
 * This is the ActiveQuery class for [[Review]].
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class ReviewQuery extends \yii\db\ActiveQuery
{
    /**
     * Select active.
     *
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['status' => Review::STATUS_ACTIVE]);

        return $this;
    }

    /**
     * Select inactive.
     *
     * @return $this
     */
    public function inactive()
    {
        $this->andWhere(['status' => Review::STATUS_INACTIVE]);

        return $this;
    }

    public function moderation()
    {
        $this->andWhere(['status' => Review::STATUS_MODERATION]);

        return $this;
    }

    public function last()
    {
        return $this->select(['id'=>'MAX(`id`)'])->one();
    }

    /**
     * Language.
     *
     * @return $this
     */
    public function language()
    {
        if (Yii::$app->setting->get('system.frontendMultilanguage')) {
            $this->andWhere(['language' => Yii::$app->language]);
        }

        return $this;
    }
}
