<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\review\models;

use Yii;

/**
 * Header and footer used in frontend depending application language.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Content extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%review_content}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['footer', 'header'], 'string'],
            [['meta_title', 'meta_description'], 'string', 'max' => 255],
            ['language', 'string', 'max' => 5],
            [
                'language',
                'unique',
                'message' => Yii::t('review', 'Content for this language is already exists'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'language' => Yii::t('review', 'Language'),
            'header' => Yii::t('review', 'Header'),
            'footer' => Yii::t('review', 'Footer'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new ContentQuery(get_called_class());
    }
}
