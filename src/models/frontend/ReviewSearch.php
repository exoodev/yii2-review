<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\review\models\frontend;

use Yii;
use yii\data\ActiveDataProvider;
use exoo\review\models\Review;

/**
 * ReviewSearch is the class used to search and filter reviews.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class ReviewSearch extends Review
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at'], 'integer'],
            ['author', 'string'],
        ];
    }

    /**
     * Returns instance of the class ActiveDataProvider with introduced search query.
     * @param array $params Search parameters.
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Review::find()->active();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => Yii::$app->settings->get('review', 'count_on_page'),
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'author' => $this->author,
        ]);

        return $dataProvider;
    }
}
