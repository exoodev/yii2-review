<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\review\models\frontend;

use Yii;
use exoo\review\models\Review;
use exoo\system\validators\ReCaptchaValidator;

/**
 * ReviewForm is the class for extension Review.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class ReviewForm extends \yii\base\Model
{
    public $author;
    public $comment;
    public $phone;
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author', 'comment'], 'required'],
            ['author', 'string', 'max' => 255],
            ['phone', 'string', 'max' => 20],
            ['comment', 'string'],
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'author' => Yii::t('review', 'Your name'),
            'comment' => Yii::t('review', 'Review'),
            'phone' => Yii::t('review', 'Phone number'),
            'email' => Yii::t('review', 'Email'),
        ];
    }

    /**
     * Saves review.
     * @return boolean whether the review was saved.
     */
    public function save($validate = true)
    {
        if ($validate && !$this->validate()) {
            return false;
        }

        $review = new Review();
        $review->author = $this->author;
        $review->comment = $this->comment;
        $review->phone = $this->phone;
        $review->email = $this->email;

        if (Yii::$app->settings->get('review', 'moderation')) {
            $review->status = Review::STATUS_MODERATION;
            $message = 'Review will be published after moderation';
        } else {
            $review->status = Review::STATUS_ACTIVE;
            $message = 'Review successfully published';
        }

        Yii::$app->session->setFlash('alert.success', Yii::t('review', $message));

        if ($review->save()) {
            return $review;
        }

        return false;
    }
}
