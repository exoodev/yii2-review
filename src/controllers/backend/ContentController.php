<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\review\controllers\backend;

use Yii;
use exoo\review\models\Content;
use exoo\review\models\backend\ContentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;

/**
 * Manages content.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class ContentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'index' => ['get'],
                    'create' => ['get', 'post'],
                    'update' => ['get', 'post'],
                    'batchDelete' => ['post'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all [[Content]] models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new [[Content]] model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Content();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('notify.success', Yii::t('review', 'Content created'));

            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing [[Content]] model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id Content id.
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('notify.success', Yii::t('review', 'Content updated'));
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing [[Content]] model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id Content id.
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($this->findModel($id)->delete()) {
            Yii::$app->session->setFlash('notify.success', Yii::t('review', 'Content deleted'));
        }

        return $this->redirect(['index']);
    }

    /**
     * Deletes multiple an existing [[Content]] models.
     * @return mixed
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionBatchDelete()
    {
        if (($ids = Yii::$app->request->post('ids')) !== null) {
            if (Content::deleteAll(['id' => $ids])) {
                Yii::$app->session->setFlash('notify.success', Yii::t('review', 'Content deleted'));
            }
            return $this->redirect(['index']);
        } else {
            throw new BadRequestHttpException(400);
        }
    }

    /**
     * Finds the [[Content]] model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id Content id.
     * @return Content The loaded model.
     * @throws NotFoundHttpException If the model is not found.
     */
    protected function findModel($id)
    {
        if (($model = Content::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('review', 'The requested page does not exist.'));
        }
    }
}
