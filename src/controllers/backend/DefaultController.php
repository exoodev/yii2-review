<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\review\controllers\backend;

use Yii;
use exoo\review\models\backend\Review;
use exoo\review\models\backend\ReviewSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use exoo\review\models\Settings;
use exoo\status\StatusAction;
use exoo\settings\actions\SettingsAction;

/**
 * Manages reviews.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'index' => ['get'],
                    'create' => ['get', 'post'],
                    'update' => ['get', 'post'],
                    'batchDelete' => ['post'],
                    'delete' => ['post'],
                    'status' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'settings' => [
                'class' => SettingsAction::class,
                'modelClass' => Settings::class,
            ],
            'status' => [
                'class' => StatusAction::class,
                'modelClass' => Review::class,
            ],
        ];
    }

    /**
     * Lists all [[Review]] models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReviewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'statuses' => Review::getStatuses(),
        ]);
    }

    /**
     * Creates a new [[Review]] model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $review = new Review([
            'scenario' => Review::SCENARIO_CREATE,
        ]);

        if ($review->load(Yii::$app->request->post()) && $review->save()) {
            Yii::$app->session->setFlash('notify.success', Yii::t('review', 'Review created'));

            return $this->redirect(['update', 'id' => $review->id]);
        }

        return $this->render('form', [
            'review' => $review,
        ]);
    }

    /**
     * Updates an existing [[Review]] model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id Review id.
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $review = $this->findModel($id);
        $review->scenario = Review::SCENARIO_UPDATE;

        if ($review->load(Yii::$app->request->post()) && $review->save()) {
            Yii::$app->session->setFlash('notify.success', Yii::t('review', 'Review updated'));
        }

        return $this->render('form', [
            'review' => $review,
        ]);
    }

    /**
     * Changing the status of the [[Review]] model on the opposite.
     * @param integer $id Review id.
     * @return mixed
     */
    public function actionStatus($id)
    {
        $model = $this->findModel($id);
        if ($model->isActive) {
            $model->status = Review::STATUS_INACTIVE;
            $action = 'unpublished';
        } else {
            $model->status = Review::STATUS_ACTIVE;
            $action = 'published';
        }
        if ($model->save()) {
            Yii::$app->session->setFlash('notify.success', Yii::t('review', 'Review ' . $action));
        }

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing [[Review]] model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($this->findModel($id)->delete()) {
            Yii::$app->session->setFlash('notify.success', Yii::t('review', 'Review deleted'));
        }

        return $this->redirect(['index']);
    }

    /**
     * Deletes multiple an existing [[Review]] models.
     * @return mixed
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionBatchDelete()
    {
        if (($ids = Yii::$app->request->post('ids')) !== null) {
            if (Review::deleteAll(['id' => $ids])) {
                Yii::$app->session->setFlash('notify.success', Yii::t('review', 'Reviews deleted'));
            }
            return $this->redirect(['index']);
        } else {
            throw new BadRequestHttpException(400);
        }
    }

    /**
     * Finds the [[Review]] model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id Review id.
     * @return Review The loaded model.
     * @throws NotFoundHttpException If the model is not found.
     */
    protected function findModel($id)
    {
        if (($model = Review::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
