<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\review\controllers\frontend;

use Yii;
use exoo\review\models\frontend\ReviewForm;
use exoo\review\models\frontend\ReviewSearch;
use exoo\review\models\Content;
use yii\web\Controller;
use yii\web\Response;
use exoo\uikit\ActiveForm;

/**
 * Default controll for reviews.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class DefaultController extends Controller
{
    /**
     * Displayes all reviews.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReviewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $reviewContent = Content::find()->language()->one();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'reviewContent' => $reviewContent,
        ]);
    }

    public function actionAdd()
    {

        if (Yii::$app->request->isAjax) {

            $model = new ReviewForm();

            if ($model->load(Yii::$app->request->post())) {
                if ($review = $model->save()) {
                    return $this->redirect(['index']);
                }
            }
            Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
            Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
            Yii::$app->assetManager->bundles['yii\widgets\ActiveFormAsset'] = false;
            Yii::$app->assetManager->bundles['yii\validators\ValidationAsset'] = false;

            return $this->renderAjax('add', [
                'model' => $model
            ]);
        }

        return $this->redirect(['index']);
    }

    /**
     * Validation form.
     */
    public function actionValidation()
    {
        $model = new ReviewForm();
        $result = [];
        foreach ($model->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($model, $attribute)] = $errors;
        }

        return $this->asJson(['validation' => $result]);
    }
}
