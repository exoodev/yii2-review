<?php

use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\Html;
use exoo\uikit\ListView;

$this->title = Html::encode(Yii::$app->settings->get('review', 'title'));
$this->params['heading'] = $this->title;

if ($reviewContent) {
    if ($reviewContent->meta_title) {
        $this->title = $reviewContent->meta_title;
    }

    if ($reviewContent->meta_description) {
        $this->registerMetaTag([
            'name' => 'description',
            'content' => $reviewContent->meta_description,
        ]);
    }
}
$columns = Yii::$app->settings->get('review', 'count_column', 1);
?>
<a id="review-add" class="uk-button uk-button-primary uk-margin-medium" href="<?= Url::to(['/review/default/add']) ?>" ex-modal='{"width":900}'>Добавить</a>

<?php if ($reviewContent && $reviewContent->header): ?>
    <?= $reviewContent->header ?>
<?php endif; ?>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_item',
    'emptyText' => Yii::t('review', 'There are no reviews'),
    'layout' => "<div class=\"uk-child-width-1-$columns@m\" uk-grid=\"masonry: true\">{items}</div>\n{pager}",
]) ?>

<?php if ($reviewContent && $reviewContent->footer): ?>
    <div class="uk-margin-top">
        <?= $reviewContent->footer ?>
    </div>
<?php endif; ?>
