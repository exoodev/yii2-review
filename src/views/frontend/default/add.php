<?php
use yii\helpers\Html;
use exoo\uikit\ActiveForm;

?>
<h3><?= Yii::t('review', 'Add review') ?></h3>
<?php $form = ActiveForm::begin([
    'id' => 'reviewForm',
    'enableAjaxValidation' => true,
    'validationUrl' => ['validation'],
    'enableAjaxSubmit' => true,
]); ?>
<div class="uk-grid-small uk-margin" uk-grid>
    <div class="uk-width-1-3@m">
        <?= $form->field($model, 'author')->textInput([
            'maxlength' => 50,
            'class' => 'uk-width-1-1',
            'placeholder' => $model->getAttributeLabel('author'),
        ])->label(false) ?>
        <?= $form->field($model, 'phone')->textInput([
            'maxlength' => 20,
            'class' => 'uk-width-1-1',
            'placeholder' => $model->getAttributeLabel('phone'),
        ])->label(false) ?>
        <?= $form->field($model, 'email')->textInput([
            'maxlength' => 255,
            'placeholder' => 'E-mail',
            'class' => 'uk-width-1-1',
        ])->label(false) ?>
    </div>
    <div class="uk-width-expand@m">
        <?= $form->field($model, 'comment')->textarea([
            'rows' => 10,
            'class' => 'uk-width-1-1',
            'placeholder' => $model->getAttributeLabel('comment'),
        ])->label(false) ?>
    </div>
</div>
<div class="uk-margin-top">
    <?= Html::submitButton(Yii::t('review', 'Send'), [
        'class' => 'uk-button uk-button-primary'
    ]) ?>
    <button class="uk-button uk-button-default uk-modal-close" type="button"><?= Yii::t('review', 'Close') ?></button>
</div>
<?php ActiveForm::end(); ?>
