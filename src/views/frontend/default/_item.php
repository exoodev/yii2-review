<?php
use yii\helpers\Html;
?>
<div class="uk-card uk-card-default uk-card-body">
    <article class="uk-comment">
        <header class="uk-comment-header uk-grid-medium uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-comment-avatar" src="<?= $this->context->module->avatar ?>" alt="" width="60">
            </div>
            <div class="uk-width-expand">
                <h4 class="uk-comment-title uk-margin-small-bottom"><?= Html::encode($model->author) ?></h4>
                <div class="uk-comment-meta">
                    <?= Yii::$app->formatter->asDate($model->created_at) ?>
                </div>
            </div>
        </header>
        <div class="uk-comment-body">
            <p><?= Html::encode($model->comment) ?></p>
        </div>
        <?php if ($model->answer): ?>
            <div class="uk-alert uk-alert-primary">
                <h5><?= $model->getAttributeLabel('answer') ?></h5>
                <p><?= Html::encode($model->answer) ?></p>
            </div>
        <?php endif; ?>
    </article>
</div>
