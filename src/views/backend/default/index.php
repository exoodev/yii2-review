<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use exoo\grid\GridView;

$this->title = Yii::t('review', 'Manage reviews');
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'containerOptions' => ['class' => 'uk-card uk-card-default'],
    'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
    'buttons' => [
        Html::a(Yii::t('review', 'Create'), ['create'], [
            'class' => 'uk-button uk-button-success'
        ]),
        Html::a(Yii::t('review', 'Delete'), ['batch-delete'], [
            'class' => 'uk-button uk-button-danger uk-hidden',
            'data' => [
                'method' => 'post',
                'confirm' => Yii::t('system', 'Are you sure you want to delete the selected items?'),
            ],
            'ex-selected' => true
        ]),
    ],
    'columns' => [
        ['class' => 'exoo\grid\CheckboxColumn'],
        [
            'attribute' => 'id',
            'headerOptions' => ['class' => 'uk-table-shrink'],
        ],
        [
            'attribute' => 'author',
            'format' => 'html',
            'value' => function($model) {
                return Html::a(Html::encode($model->author), ['update', 'id' => $model->id])
                    . '<span class="uk-text-small uk-text-muted"> | ' . Yii::$app->formatter->asDatetime($model->created_at) . '</span>'
                    . '<div class="uk-overflow-auto uk-margin-small"><div class="uk-text-small uk-text-muted" style="max-height:150px;">' . Html::encode($model->comment) . '</div></div>';
            },
        ],
        'email',
        'phone',
        // 'created_at:datetime',

        ['class' => 'exoo\status\StatusColumn'],
    ],
]); ?>
