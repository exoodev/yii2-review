<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use exoo\uikit\ActiveForm;
use exoo\datepicker\DatePicker;
use exoo\timepicker\TimePicker;

$this->title = Yii::t('review', 'Review');
?>

<?php $form = ActiveForm::begin(); ?>
    <div class="tm-sticky-subnav uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div>
            <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
        </div>
        <div>
            <?= Html::submitButton(Yii::t('system', 'Save'), [
                'class' => 'uk-button uk-button-success'
            ]) ?>
            <?= Html::a(Yii::t('system', 'Close'), ['index'], [
                'class' => 'uk-button uk-button-default'
            ]) ?>
            <?php if (!$review->isNewRecord): ?>
                <?= Html::a(Yii::t('system', 'Delete'), ['delete', 'id' => $review->id], [
                    'data-method' => 'post',
                    'data-confirm' => Yii::t('blog', 'Are you sure want to delete this post?'),
                    'class' => 'uk-button uk-button-danger',
                ]) ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="uk-card uk-card-default uk-card-body">
        <div uk-grid>
            <div class="uk-width-expand@m">
                <?= $form->field($review, 'author')->textInput(['maxlength' => 255]) ?>
                <?= $form->field($review, 'email') ?>
                <?= $form->field($review, 'phone') ?>
            </div>
            <div class="uk-width-1-2@m">
                <div class="uk-margin">
                    <?= Html::label($review->getAttributeLabel('created_at'), null, ['class' => 'uk-form-label']) ?>
                    <div class="uk-form-controls">
                        <div class="uk-grid-small" uk-grid>
                        <div class="uk-width-1-2">
                            <?= $form->field($review, 'creationDate')->widget(DatePicker::className())->label(false)->icon('<i class="far fa-calendar-alt"></i>') ?>
                            </div>
                            <div class="uk-width-1-2">
                                <?= $form->field($review, 'creationTime')->widget(TimePicker::className(), [
                                    'timeFormat' => 'short',
                                ])->label(false)->icon('<i class="far fa-clock"></i>') ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?= $form->field($review, 'status')->dropDownlist($review->statusList) ?>
            </div>
        </div>
        <?= $form->field($review, 'comment')->textarea(['rows' => 10]) ?>
        <?= $form->field($review, 'answer')->textarea(['rows' => 10]) ?>
    </div>
<?php ActiveForm::end(); ?>
