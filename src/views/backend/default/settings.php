<?php

use yii\helpers\Html;
use yii\helpers\Url;
use exoo\uikit\ActiveForm;

$this->title = Yii::t('review', 'Settings');
?>

<div class="uk-panel uk-panel-box uk-panel-box-secondary">
    <?php $form = ActiveForm::begin(); ?>
        <div class="tm-sticky-subnav uk-flex uk-flex-between" uk-margin>
            <div>
                <div class="uk-card-title"><?= Html::encode($this->title) ?></div>
            </div>
            <div>
                <?= Html::submitButton(Yii::t('system', 'Save'), [
                    'class' => 'uk-button uk-button-success'
                ]) ?>
            </div>
        </div>
        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'count_on_page')->textInput(['type' => 'number', 'min' => 1, 'max' => 30, 'class' => 'uk-form-width-small']) ?>
        <?= $form->field($model, 'count_column')->textInput(['type' => 'number', 'min' => 1, 'max' => 6, 'class' => 'uk-form-width-small']) ?>
        <?= $form->field($model, 'moderation')->toggle() ?>
    <?php ActiveForm::end(); ?>
</div>
