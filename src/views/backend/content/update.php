<?php

$this->title = Yii::t('review', 'Updating content');
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>
