<?php

$this->title = Yii::t('review', 'Content creation');
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>
