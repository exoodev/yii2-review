<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use exoo\uikit\ActiveForm;
use exoo\htmleditor\HtmlEditor;

$locales =Yii::$app->locale->getLanguageList();
?>

<?php $form = ActiveForm::begin(); ?>
    <div class="tm-sticky-subnav uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div>
            <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
        </div>
        <div>
            <?= Html::submitButton(Yii::t('system', 'Save'), [
                'class' => 'uk-button uk-button-success'
            ]) ?>
            <?= Html::a(Yii::t('system', 'Close'), ['index'], [
                'class' => 'uk-button uk-button-default'
            ]) ?>
            <?php if (!$model->isNewRecord): ?>
                <?= Html::a(Yii::t('system', 'Delete'), ['delete', 'id' => $model->id], [
                    'data-method' => 'post',
                    'data-confirm' => Yii::t('blog', 'Are you sure want to delete this post?'),
                    'class' => 'uk-button uk-button-danger',
                ]) ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="uk-card uk-card-default uk-card-body">
        <?= $form->field($model, 'language')->dropDownlist($locales, ['prompt' => '']) ?>
        <?= $form->field($model, 'header')->widget(HtmlEditor::className()) ?>
        <?= $form->field($model, 'footer')->widget(HtmlEditor::className()) ?>
        <?= $form->field($model, 'meta_title')->textInput([
            'maxlength' => 255,
            'class' => 'uk-width-1-1',
        ]) ?>
        <?= $form->field($model, 'meta_description')->textarea([
            'rows' => 3, 'maxlength' => 255,
            'class' => 'uk-width-1-1',
        ]) ?>
    </div>
<?php ActiveForm::end(); ?>
