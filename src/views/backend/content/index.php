<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use exoo\grid\GridView;
use exoo\grid\ActionColumn;

$this->title = Yii::t('review', 'Manage content');
$languages =Yii::$app->locale->getLanguageList();
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'containerOptions' => ['class' => 'uk-card uk-card-default'],
    'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
    'buttons' => [
        Html::a(Yii::t('review', 'Create'), ['create'], [
            'class' => 'uk-button uk-button-success'
        ]),
        Html::a(Yii::t('review', 'Delete'), ['batch-delete'], [
            'class' => 'uk-button uk-button-danger uk-hidden',
            'data-ex-grid-action' => [
                'confirm' => Yii::t('system', 'Are you sure you want to delete the selected items?'),
            ],
        ]),
    ],
    'columns' => [
        [
            'class' => 'exoo\grid\CheckboxColumn',
        ],
        [
            'attribute' => 'id',
            'headerOptions' => ['class' => 'uk-table-shrink'],
        ],
        [
            'attribute' => 'language',
            'filter' => $languages,
            'filterInputOptions' => ['prompt' => 'Все', 'class' => 'uk-width-1-1'],
            'format' => 'raw',
            'value' => function ($model) use ($languages) {
                if (array_key_exists($model->language, $languages)) {
                    return $languages[$model->language];
                }
                return $model->language;
            },
        ],
        [
            'class' => ActionColumn::className(),
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    return Html::a(Html::icon('uk-icon-pencil'), $url, [
                        'class' => 'uk-button uk-button-primary',
                        'data-uk-tooltip' => ['pos' => 'right'],
                        'title' => Yii::t('review', 'Update'),
                    ]);
                },
            ],
            'template' => '{update}',
        ],
    ],
]); ?>
