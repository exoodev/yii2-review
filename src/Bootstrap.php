<?php

namespace exoo\review;

use Yii;
use yii\base\BootstrapInterface;

/**
 * Review extension bootstrap class.
 */
class Bootstrap implements BootstrapInterface
{
	/**
     * @inheritdoc
     */
	public function bootstrap($app)
	{
		$app->i18n->translations['review'] = [
			'class' => 'yii\i18n\PhpMessageSource',
			'basePath' => '@exoo/review/messages',
		];

		if (!$app->getModule('review')->isBackend) {
			Yii::$app->getUrlManager()->addRules([
				'reviews' => 'review/default/index',
			], false);
        }
	}
}
