<?php

$subnav = [
    [
        'label' => Yii::t('review', 'Reviews'),
        'url' => ['/review/default/index'],
        'controller' => 'default',
    ],
    [
        'label' => Yii::t('review', 'Content'),
        'url' => ['/review/content/index'],
        'controller' => 'content',
    ],
    [
        'label' => Yii::t('review', 'Settings'),
        'url' => ['/review/default/settings'],
        'controller' => 'setting',
    ]
];

return [
    'navside' => [
        [
            'icon' => '<i class="fas fa-comments fa-lg fa-fw uk-margin-small-right"></i>',
            'label' => Yii::t('review', 'Reviews'),
            'url' => ['/review/default/index'],
            'items' => $subnav,
        ]
    ],
    'subnav' => $subnav,
];
