<?php
use yii\helpers\Html;
?>

<div uk-slider>
    <div class="uk-position-relative uk-visible-toggle" tabindex="-1">
        <div class="uk-slider-container">
            <ul class="uk-slider-items uk-child-width-1-2@s uk-grid">
                <?php foreach ($reviews as $review) : ?>
                <li>
                    <div class="tm-review">
                        <article class="uk-comment">
                            <header class="uk-comment-header uk-grid-medium uk-flex-middle" uk-grid>
                                <div class="uk-width-auto">
                                    <img class="uk-comment-avatar" src="<?= Yii::$app->getModule('review')->avatar ?>" width="50" height="50" alt="">
                                </div>
                                <div class="uk-width-expand uk-text-left">
                                    <h4 class="uk-comment-title uk-margin-remove"><?= Html::encode($review->author) ?></h4>
                                    <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                                        <li><?= Yii::$app->formatter->asDate(Html::encode($review->created_at)) ?></li>
                                    </ul>
                                </div>
                            </header>
                            <div class="uk-comment-body uk-height-small tm-scrollbar" uk-overflow-auto>
                                <p>"<em><?= Html::encode($review->comment) ?></em>"</p>
                            </div>
                        </article>
                    </div>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="uk-hidden@s uk-light">
            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>

        <div class="uk-visible@s">
            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>
    </div>

    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin uk-light"></ul>
</div>
