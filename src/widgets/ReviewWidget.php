<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2019
 * @package
 * @version 1.0.0
 */

namespace exoo\review\widgets;

use yii\base\Widget;
use exoo\review\models\Review;

/**
 * Undocumented class.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class ReviewWidget extends Widget
{
    /**
     * @var array
     */
    public $items = [];
    /**
     * @var integer the limit reviews
     */
    public $limit;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->items = Review::find()->active()->limit($this->limit)->all();
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return $this->render('items', ['reviews' => $this->items]);
    }
}
